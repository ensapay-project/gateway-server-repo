# CMI Gateway Server

Gateway Server is the component for routing external requests to the corresponding CMI microservice.

## Requirments
Please install these requirements before setting up the porject on your local machine :

| Requirement | DESCRIPTION                                                                         |
| ----------- | ----------------------------------------------------------------------------------- |
| JDK 8       | At least JDK 8 is required to compile the services                                  |
| Maven       | Java application packaging system   |
| Consul    | Discovery and config server for services |
| Docker    | For deploying in Docker |
## Usage

Services are registered in `application.yml` file :
```yaml
spring:
  cloud:
    gateway:
      routes:
        - id: my-service
          uri: my-service-uri
          predicates:
            - Path=/paths-to-my-service/**
```
In case you use `Consul` for services discovery, you can use your **service name** instead of URI :
```yaml
spring:
  cloud:
    gateway:
      routes:
        - id: my-service
          uri: lb://my-service-name
          predicates:
            - Path=/paths-to-my-service/**
```
## Deloyment
To deploy in Docker, just run `deploy.sh` script.
## Author

LAHMIDI Oussama


